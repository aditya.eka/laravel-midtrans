<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone')->unique();
            $table->string('company')->unique();
            $table->string('company_description')->nullable();
            $table->string('company_address');
            $table->integer('company_negara_id')->unsigned()->nullable();
            $table->integer('company_kotakabupaten_id')->unsigned()->nullable();
            $table->integer('company_kecamatan_id')->unsigned()->nullable();
            $table->integer('company_kelurahan_id')->unsigned()->nullable();
            $table->string('company_zip_code')->nullable();
            $table->string('company_website')->nullable();
            $table->string('company_logo')->nullable();
            $table->string('company_npwp')->nullable();
            $table->string('company_siup')->nullable();
            $table->string('company_phone')->nullable();
            $table->string('company_fax')->nullable();
            $table->string('company_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tenants');
    }
};
