<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

Route::get('/', [OrderController::class, 'index']);
Route::get('/subscription', [OrderController::class, 'subscription']);
Route::post('/checkout', [OrderController::class, 'checkout']);

Route::get('/invoice/{id}', [OrderController::class, 'invoice']);