<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function subscription()
    {
        return view('subscription');
    }

    public function checkout(Request $request)
    {

        $price = 250000;
        if ($request->qty == 1) {
            $price = 250000;
        } elseif ($request->qty == 2) {
            $price = 800000;
        } else {
            $price = 2500000;
        }
        $request['total_price'] = $price;
        $request['status'] = 'Unpaid';

        $order = Order::create($request->all());

        // split $request->name into first_name and last_name
        $first_name = explode(' ', $request->name)[0];
        $last_name = explode(' ', $request->name, 2)[1];
        /*Install Midtrans PHP Library (https://github.com/Midtrans/midtrans-php)
        composer require midtrans/midtrans-php

        Alternatively, if you are not using **Composer**, you can download midtrans-php library
        (https://github.com/Midtrans/midtrans-php/archive/master.zip), and then require
        the file manually.

        require_once dirname(__FILE__) . '/pathofproject/Midtrans.php'; */

        //SAMPLE REQUEST START HERE

        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = config('midtrans.server_key');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $params = array(
            'transaction_details' => array(
                'order_id' => $order->id,
                'gross_amount' => $order->total_price,
            ),
            'customer_details' => array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $request->email,
                'phone' => $request->phone,
            ),
        );

        $snapToken = \Midtrans\Snap::getSnapToken($params);
        // return view('checkout', compact('snapToken'));
        // return view checkout with $order
        return view('checkout', compact('snapToken', 'order'));
    }

    public function midtransCallback(Request $request)
    {
        $serverKey = config('midtrans.server_key');
        $hashed = hash('sha512', $request->order_id . $request->status_code . $request->gross_amount . $serverKey);

        if ($hashed == $request->signature_key) {
            // if ($request->transaction_status == 'capture') {
            //     $order = Order::find($request->order_id);
            //     $order->update([
            //         'status' => 'Paid',
            //     ]);

            //     // if status successfully updated, redirect to invoice page with order id
            //     return redirect()->route('invoice', $order->id);
            // }
            dd($request->all());
        }
    }

    public function invoice($id)
    {
        $order = Order::find($id);
        return view('invoice', compact('order'));
    }
}
