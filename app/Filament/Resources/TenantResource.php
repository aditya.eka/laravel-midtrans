<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TenantResource\Pages;
use App\Filament\Resources\TenantResource\RelationManagers;
use App\Models\Tenant;
use Filament\Forms;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class TenantResource extends Resource
{
    protected static ?string $model = Tenant::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Section::make()
                    ->schema([
                        Section::make('Informasi Pengguna')
                            ->columns(2)
                            ->collapsible()
                            ->schema([
                                TextInput::make('name')
                                    ->label('Nama Pengguna')
                                    ->required()
                                    ->maxLength(255)
                                    ->placeholder('Contoh: CRM Awesome'),
                                TextInput::make('email')
                                    ->label('Email Pengguna')
                                    ->email()
                                    ->required()
                                    ->maxLength(255)
                                    ->placeholder('Contoh: mail@mail.com'),
                                TextInput::make('password')
                                    ->label('Kata Sandi')
                                    ->password()
                                    ->required()
                                    ->maxLength(255)
                                    ->placeholder('Masukkan kata sandi'),
                                TextInput::make('phone')
                                    ->label('Nomor Telepon')
                                    ->tel()
                                    ->required()
                                    ->maxLength(255)
                                    ->placeholder('Contoh: 081234567890'),
                            ]),
                        Section::make('Informasi Perusahaan')
                        ->collapsible()
                        ->collapsed()
                            ->schema([
                                TextInput::make('company')
                                    ->label('Nama Perusahaan')
                                    ->required()
                                    ->maxLength(255),
                                TextInput::make('company_address')
                                    ->required()
                                    ->maxLength(255),
                                TextInput::make('company_negara_id')
                                    ->numeric(),
                                TextInput::make('company_kotakabupaten_id')
                                    ->numeric(),
                                TextInput::make('company_kecamatan_id')
                                    ->numeric(),
                                TextInput::make('company_kelurahan_id')
                                    ->numeric(),
                                TextInput::make('company_zip_code')
                                    ->maxLength(255),
                                TextInput::make('company_website')
                                    ->maxLength(255),
                                TextInput::make('company_logo')
                                    ->maxLength(255),
                                TextInput::make('company_npwp')
                                    ->maxLength(255),
                                TextInput::make('company_siup')
                                    ->maxLength(255),
                                TextInput::make('company_phone')
                                    ->tel()
                                    ->maxLength(255),
                                TextInput::make('company_fax')
                                    ->maxLength(255),
                                TextInput::make('company_email')
                                    ->email()
                                    ->maxLength(255),

                            ])
                            ->columns(2)
                            ->collapsible()
                            ->collapsed(),
                            Section::make('Deskripsi Perusahaan')
                            ->schema([
                                Textarea::make('company_description')
                                    ->maxLength(255)
                                    ->rows(3)
                                    ->placeholder('Deskripsi perusahaan'),
                            ])
                            ->columns(1)
                            ->collapsible()
                            ->collapsed(),
                    ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->searchable(),
                Tables\Columns\TextColumn::make('email')
                    ->searchable(),
                Tables\Columns\TextColumn::make('email_verified_at')
                    ->dateTime()
                    ->sortable(),
                Tables\Columns\TextColumn::make('phone')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_description')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_address')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_negara_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('company_kotakabupaten_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('company_kecamatan_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('company_kelurahan_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('company_zip_code')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_website')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_logo')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_npwp')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_siup')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_phone')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_fax')
                    ->searchable(),
                Tables\Columns\TextColumn::make('company_email')
                    ->searchable(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTenants::route('/'),
            'create' => Pages\CreateTenant::route('/create'),
            'edit' => Pages\EditTenant::route('/{record}/edit'),
        ];
    }
}
