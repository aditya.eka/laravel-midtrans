{{-- create invoice view with data from $order --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container">
        <h1 class="my-3">Invoice Vendor</h1>
        <div class="card my-3" style="width: 25rem;">
            <div class="card-body">
                <img src="https://cloud.atlasdigitalize.com/images/logo/logo.png" style="width:75%; margin:bottom: 10">
                @php
                    $subscription = '';

                    if ($order->qty == 1) {
                        $subscription = 'Monthly (1 User) - Rp 250.000';
                    } elseif ($order->qty == 2) {
                        $subscription = 'Quarterly (2 Users) - Rp 800.000';
                    } else {
                        $subscription = 'Yearly (3 Users) - Rp 2.500.000';
                    }
                @endphp
                <h5 class="card-title">Thank You For Your Subscription {{ $subscription }}</h5>
                <div class="mb-3">

                    <label for="name" class="form-label">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Order Name"
                        value="{{ $order->name }}" readonly>

                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" name="phone" class="form-control" id="phone" placeholder="123 4567 890"
                        value="{{ $order->phone }}" readonly>

                    <label for="address" class="form-label">Address</label>
                    <input type="text" name="address" class="form-control" id="address"
                        placeholder="Detailed Address" value="{{ $order->address }}" readonly>

                    <label for="total" class="form-label">Total</label>
                    <input type="text" name="total" class="form-control" id="total"
                        value="{{ $order->total_price }}" readonly>

                    <label for="status" class="form-label">Status</label>
                    <input type="text" name="status" class="form-control" id="status"
                        value="{{ $order->status }}" readonly>

                </div>
                <a href="/"><button class="btn btn-flat"
                        style="background-color:teal;color:white;">Back</button></a>
            </div>
        </div>
</body>

</html>
