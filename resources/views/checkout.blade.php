<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Payment</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h1 class="my-3">Test Payment</h1>
        <div class="card my-3" style="width: 25rem;">
            <img src="https://cloud.atlasdigitalize.com/images/logo/logo.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Order Detail</h5>
                <table>
                    <tr>
                        <td>Subscription Type</td>
                        <td>:</td>
                        <td>
                            @if ($order->qty == 1)
                                Monthly (1 User) - Rp 250.000
                            @elseif ($order->qty == 2)
                                Quarterly (2 Users) - Rp 800.000
                            @else
                                Yearly (3 Users) - Rp 2.500.000
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td>{{ $order->name }}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>:</td>
                        <td>{{ $order->phone }}</td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>:</td>
                        <td>{{ $order->address }}</td>
                    </tr>
                </table>
                <button class="btn btn-primary mt-3" id="pay-button" style="background-color:teal;color:white;">Checkout
                    Now</button>
                &nbsp;
                {{-- add some information --}}
                <p class="text-muted mt-3"><b>Choose Credit/Debit Card</b></p>
                <p class="text-muted mt-3">Card Number 4811 1111 1111 1114</p>
                <p class="text-muted mt-3">Exp Date 02/25</p>
                <p class="text-muted mt-3">CVV 123</p>
                <p class="text-muted mt-3">3D Secure PIN 112233</p>
            </div>
        </div>
    </div>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="{{ config('midtrans.client_key') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    var payButton = document.getElementById('pay-button');
    payButton.addEventListener('click', function() {
        // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
        window.snap.pay('{{ $snapToken }}', {
            onSuccess: function(result) {
                // swal and return to invoice page with order_id
                Swal.fire({
                    icon: 'success',
                    title: 'Payment Success',
                    text: 'Your payment is successful!'
                }).then(function() {
                    window.location = '/invoice/' + result.order_id;
                });

            },
            onPending: function(result) {
                Swal.fire({
                    icon: 'info',
                    title: 'Payment Pending',
                    text: 'Your payment is pending!'
                })
            },
            onError: function(result) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Payment failed!',
                    footer: '<a href>Why do I have this issue?</a>'
                })
            },
            onClose: function() {
                Swal.fire({
                    icon: 'info',
                    title: 'Payment Closed',
                    text: 'You have closed the payment window'
                })
            }
        })
    });
</script>
</body>

</html>
