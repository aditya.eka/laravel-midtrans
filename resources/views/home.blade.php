<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Payment</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container">
        <div class="card my-3" style="width: 18rem;">
            <img src="https://cloud.atlasdigitalize.com/images/logo/logo.png" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">CRM Subscription</h5>
                <p class="card-text">Choose your subscription type</p>
                <form action="/checkout" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="qty" class="form-label">Subscription Type</label>
                        <select name="qty" id="qty" class="form-control">
                            <option value="1">Monthly (1 User) - Rp 250.000</option>
                            <option value="2">Quarterly (2 Users) - Rp 800.000</option>
                            <option value="3">Yearly (3 Users) - Rp 2.500.000</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" name="name" class="form-control" id="name"
                            placeholder="Order Name">
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" name="phone" class="form-control" id="phone"
                            placeholder="123 4567 890">
                    </div>
                    <div class="mb-3">
                        <label for="address" class="form-label">Address</label>
                        <input type="text" name="address" class="form-control" id="address"
                            placeholder="Detailed Address">
                    </div>
                    <button class="btn btn-primary" type="submit"
                        style="background-color:teal;color:white;">Checkout</button>
                </form>
            </div>
        </div>
    </div>
</body>

</html>
