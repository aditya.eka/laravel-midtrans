<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Subscription</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <style>
        .product-card:hover {
            border-color: teal;
            transition: border-color 0.3s ease;
        }

        .btn-teal:hover {
            background-color: teal;
            transition: background-color 0.3s ease;
        }

        .bg-gray-900 {
            min-height: 100vh;
            /* Adjusts background to cover full viewport height */
        }

        .logo {
            width: 60%;
            height: auto;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <div class="pt-5 bg-gray-900" id="pricing">
        <div class="mx-auto pb-5 mt-4 max-w-7xl px-6 lg:px-8">
            <div class="mx-auto max-w-4xl text-center">
                <img src="https://cloud.atlasdigitalize.com/images/logo/logo.png" alt="" class="logo">
                <h1 class="text-xl font-semibold leading-7 text-teal-400">Pricing Plan & Subscription</h1>
                <div class="mx-auto mt-5 max-w-4xl border-b-2 border-teal-500"></div>
                <p class="mt-2 text-xl font-bold tracking-tight text-white sm:text-2xl">"Temukan keunggulan kompetitif
                    dengan paket langganan CRM kami yang inovatif, dirancang untuk meningkatkan produktivitas,
                    memperluas jaringan, dan menghasilkan pertumbuhan yang berkelanjutan bagi bisnis Anda"</p>
            </div>
            <div class="mx-auto mt-5 max-w-2xl border-b-2 border-teal-500"></div>
            <p class="mx-auto mt-6 max-w-2xl text-center text-lg leading-8 text-gray-300">Pilih dari berbagai paket
                langganan fleksibel kami yang disesuaikan untuk memenuhi kebutuhan bisnis Anda.</p>
            <div class="isolate mx-auto mt-10 grid max-w-md grid-cols-1 gap-8 lg:mx-0 lg:max-w-none lg:grid-cols-3">
                <!-- First Product -->
                <div class="ring-1 ring-teal-500 rounded-3xl p-8 xl:p-10 product-card">
                    <div class="flex items-center justify-between gap-x-4">
                        <h2 id="product1" class="text-xl font-semibold leading-8 text-white">Standard Plan</h2>
                    </div>
                    <p class="mt-4 text-sm leading-6 text-gray-300">Standard plan untuk bisnis kecil</p>
                    <p class="mt-6 flex items-baseline gap-x-1">
                        <span class="text-3xl font-bold tracking-tight text-white">Rp 125.000 </span><span
                            class="text-sm font-semibold leading-6 text-gray-300"> / Bulan</span>
                    </p>
                    <a href="/order" aria-describedby="product1"
                        class="bg-teal-500 text-white btn-teal hover:bg-teal-600 focus-visible:outline-white mt-6 block rounded-md py-2 px-3 text-center text-sm font-semibold leading-6 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2">Order
                        Now</a>
                    <ul role="list" class="mt-8 space-y-3 text-sm leading-6 text-gray-300 xl:mt-10">
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg><b class="hover:text-teal-600">5 Users</b></li>
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>Attendance Module (Absensi)</li>
                            <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>Task/Scheduling Module</li>
                            <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>Customer Contact Module</li>
                            <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>Team Management Module</li>
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>Web, Android, IoS</li>
                    </ul>
                </div>

                <!-- Second Product -->
                <div class="ring-2 ring-teal-500 rounded-3xl p-8 xl:p-10 product-card">
                    <div class="flex items-baseline justify-between gap-x-4">
                        <h2 id="product2" class="text-lg font-semibold leading-8 text-white">Medium Plan</h2>
                        <p class="rounded-full bg-teal-500 px-2.5 py-1 text-xs font-semibold leading-5 text-white">
                            Paling Laris</p>
                    </div>
                    <p class="mt-4 text-sm leading-6 text-gray-300">Medium plan untuk bisnis menengah ke atas</p>
                    <p class="mt-6 flex items-baseline gap-x-1">
                        <span class="text-4xl font-bold tracking-tight text-white">Rp 240.000</span><span
                            class="text-sm font-semibold leading-6 text-gray-300">/Bulan</span>
                    </p>
                    <a href="/order" aria-describedby="product2"
                        class="bg-teal-500 text-white btn-teal shadow-sm hover:bg-teal-600 focus-visible:outline-white mt-6 block rounded-md py-2 px-3 text-center text-sm font-semibold leading-6 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2">Order
                        Now</a>
                    <ul role="list" class="mt-8 space-y-3 text-sm leading-6 text-gray-300 xl:mt-10">
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>10 Users</li>
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>All Standard Features</li>
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd" d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a
                                .75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z" clip-rule="evenodd">
                                </path>
                            </svg>Web, Android, IoS</li>
                    </ul>
                </div>

                <!-- Third Product -->
                <div class="ring-1 ring-teal-500 rounded-3xl p-8 xl:p-10 product-card">
                    <div class="flex items-center justify-between gap-x-4">
                        <h2 id="product3" class="text-lg font-semibold leading-8 text-white">Advance Plan</h2>
                    </div>
                    <p class="mt-4 text-sm leading-6 text-gray-300">Product details for Product Type 3</p>
                    <p class="mt-6 flex items-baseline gap-x-1">
                        <span class="text-4xl font-bold tracking-tight text-white">Rp 500.000</span><span
                            class="text-sm font-semibold leading-6 text-gray-300"> /Bulan</span>
                    </p>
                    <a href="/order" aria-describedby="product3"
                        class="bg-teal-500 text-white btn-teal hover:bg-teal-600 focus-visible:outline-white mt-6 block rounded-md py-2 px-3 text-center text-sm font-semibold leading-6 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2">Order
                        Now</a>
                    <ul role="list" class="mt-8 space-y-3 text-sm leading-6 text-gray-300 xl:mt-10">
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>20 Users</li>
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>All Standard Features</li>
                        <li class="flex gap-x-3"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true" class="h-6 w-5 flex-none text-white">
                                <path fill-rule="evenodd"
                                    d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                    clip-rule="evenodd"></path>
                            </svg>Web, Android, IoS</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
